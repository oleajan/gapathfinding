
var lifetime;
var population;
var lifecycle;
var record_time;
var target;
var obstacles = [];
var new_obstacle = null;

var chromosomes;

var path;
var points = [];
var abs_dist;

var start = false;
var optimize = false;
var mpool = false;

var targetSet = false;
var obstacleSet = false;

const targetSize = 18;

var abs_label = 0;
var h3;

function setup() {
	bg = loadImage("images/white-paper.jpg");
	createCanvas(800, 600);

	lifetime = 350;
	lifecycle = 0;
	record_time = lifetime;

	this.start_process();

	target = new Obstacle(width / 2 + 340, height / 2, targetSize, targetSize);
	abs_dist = dist(width-740, height/2, target.position.x, target.position.y);
	h3 = createElement('h3', 'Absolute Distance to Target: ' +(abs_dist*0.01).toFixed(2) +'km');

	obstacles = [];
	this.new_obs();
	
	// constant mutation rate
	const mutation_rate = 0.1;
	chromosomes = 10;
	population = new Population(mutation_rate, chromosomes);
}

function draw() {
	background(bg);

	fill(255, 0, 0);
	ellipse(target.position.x + 12, target.position.y + 12, targetSize, targetSize);

	if(start == true) {
		if (lifecycle < lifetime) {
			population.live(obstacles);
			if ((population.target_reached()) && (lifecycle < record_time)) {
				record_time = lifecycle;
			}
		lifecycle++;
		// Otherwise a new generation
		}
		else {
			lifecycle = 0;
			population.fitness();
			population.selection();
			population.reproduction();
		}
	}	

	if(obstacles.length > 0) {
		for (var i = 0; i < obstacles.length; i++) {
			obstacles[i].display();
		}
	}

	if (new_obstacle !== null) {
		new_obstacle.display();
	}

	fill(0);
	noStroke();
	text("Generation #: " + population.get_generations(), 10, 18);
	text("Time Left: " + (lifetime - lifecycle), 10, 36);
	text("Life Cycle: " + (lifecycle), 10, 54);
	text("Record: " + (record_time), 10, 73);
}

this.new_obs = function() {
	if(obstacleSet == true) {
		console.log('Pathfinding has already been started.');
	} else {
		obstacles.push(new Obstacle(random(50, width-150), random(20, 550), random(10,100), random(20,200)));
		console.log('new Obstacle generated');
		// obstacles.push(new Obstacle(width/2, height/2-100, 10, 200))
	}
}


// function for removing obstacles with a click
// invoked after the location of the target is set
this.remove_obs = function() {
	if(obstacleSet == false) {
		for(var i = 0; i < obstacles.length; i++) {
			if(obstacles[i].contains(createVector(mouseX, mouseY))){
				console.log('obstacle ['+i+']: deleted\n'+obstacles[i]);
				obstacles.splice(i, 1);
			}
		}
	} else {
		console.log('Pathfinding already started...');
	}
}

this.start_process = function() {
	console.log('-- CHEATSHEET---------------------------------------------');
	console.log('*click anywhere on the screen to move the target');
	console.log('[space] Set the Target');
	console.log('[n] Generate New Obstacle\t\t\t[m] Show Mating Pool');
	console.log('[s] Start / Pause\t\t\t\t[p] show Genes');
	console.log('[d] show DNA\t\t\t\t\t[k] show Kilometer');
	console.log('[t] get Fittest\t\t\t\t\t[f] show Fitness Normal');
	console.log('---------------------------------------------------------')
}

this.absPath = function() {
	h3.html('Absolute Distance to Target: ' +(abs_dist*0.01).toFixed(2)+ 'km');	
}


function mousePressed() {
	if(targetSet == false) {
		target.position.x = mouseX;
		target.position.y = mouseY;
		abs_dist = dist(width-740, height/2, target.position.x, target.position.y);
		this.absPath();
		console.log('Target @x: ' + target.position.x + '\t\t@y: ' +target.position.y+ '\t\t' +(abs_dist*0.01).toFixed(2) +'km');
	}
	if(targetSet == true) {
		this.remove_obs();
	}
}

function keyPressed() {

	if(key == 'l' || key == 'L') {
		start = !start;
	}

	if (key == 's' || key == 'S') {
		this.absPath();
		if (start == false) {
			console.log("START");
			start = !start;
		} else {
			console.log('PAUSE');
			start = !start;
		}
		targetSet = true;
		obstacleSet = true;
	}

	if (key == ' ') {
		if(targetSet == false) {
			console.log('target set at X: ' +target.position.x+ '\tY: ' +target.position.y);
			targetSet = true;
			this.absPath();
		}
	}

	if (key == 'm' || key == 'M') {
		console.log('--- Mating Pool ----------------------');
		population.show_mating_pool();			
		if(start != false)	start = !start;		
	}

	if (key == 'n' || key == 'N') {
			this.new_obs();
	}

	if(key == 'p' || key == 'P') {
		population.show_genes();
		if(start != false)	start = !start;
	}

	if(key == 'd' || key == 'D') {
		population.show_dna();
		if(start != false)	start = !start;
	}

	if(key == 'k' || key == 'K') {
		population.return_kilometer();
	}

	if(key == 't' || key == 'T') {
		population.get_fittest_dna();
	}
	
	if(key == '1') {
		console.log(1);
		population.draw_fittest();
	}

	if(key == 'f' || key == 'F') {
		console.log("-- Fitness Normal");
		population.get_normal_fitness();
	}

	if(key == 'o' || key == 'O') {
		console.log(obstacles.length);
	}
}