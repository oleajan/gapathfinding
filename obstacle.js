

function Obstacle(x, y, w, h) {

    this.position = createVector(x, y);
    this.w = w;
    this.h = h;

    this.display = function() {
        stroke(0);
        fill(175);
        strokeWeight(1);
        rectMode(CORNER);
        rect(this.position.x, this.position.y, this.w, this.h);
    }

    // function to check if something is in the obstacle
    this.contains = function(spot) {
        if (spot.x > this.position.x && spot.x < this.position.x+w && spot.y > this.position.y && spot.y < this.position.y +h){
            return true;
        } else {
            return false;
        }
    }
}