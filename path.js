
function Path() {
    this.points = [];

    this.add_point = function(x,y) {
        var point = createVector([x], [y]);
        this.points.push(point);
    }

    this.is_path = function() {
        if(points.length > 1) {
            return true;
        } else {
            return false;
        }
    }

    this.display = function() {
        stroke(99);
        strokeWeight(5);
        beginShape();
        for(var i = 0; i < this.points.length; i++) {
            vertex(this.points[i].x, this.points[i].y);
        }
        endShape();
    }
}