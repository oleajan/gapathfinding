
function DNA(newgenes) {

	this.maxforce = 0.1;

	if (newgenes) {
		this.genes = newgenes;
	} else {
		this.genes = [];

		for (var i = 0; i < lifetime; i++) {
			var theta = random(TWO_PI);
			this.genes[i] = createVector(cos(theta), sin(theta));
			this.genes[i].mult(random(0, this.maxforce));
		}
	}

	this.genes[0].normalize();

	//crossover function
	//takes a partner chromosome and produces a child of the number of the initial population
	//from the dna sequence of the two parent chromosomes
	this.crossover = function (partner) {
		var child = new Array(this.genes.length);
		var crossover = int(random(this.genes.length));

		for (var i = 0; i < this.genes.length; i++) {
			if (i > crossover) child[i] = this.genes[i];
			else child[i] = partner.genes[i];
		}
		var newgenes = new DNA(child);
		return newgenes;
	}

	//mutation function
	//every agent from the population will be subjected for mutation
	//mutation will occur at a probability of 0.01% for every gene in a chromosome
	this.mutate = function (m) {
		for (var i = 0; i < this.genes.length; i++) {
			if (random(1) < m) {
				var angle = random(TWO_PI);
				this.genes[i] = createVector(cos(angle), sin(angle));
				this.genes[i].mult(random(0, this.maxforce));
				if (i == 0) this.genes[i].normalize();
			}
		}
	}

	//get genes
	this.getGenes = function () {
		return this.genes;
	}
}