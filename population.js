
function Population(m, num) {

	this.mutation_rate = m;
	this.population = new Array(num);
	this.mating_pool = [];
	this.normal_fitness = [];
	this.n = [];
	this.generations = 0;
	this.index;
	this.kilometers;
	this.starting_point = createVector(width-760, height/2);
	
	this.run = function () {
		this.fitness();
		this.selection();
		this.reproduction();
	}

	for (var i = 0; i < this.population.length; i++) {

		console.log('current population: ' + this.population.length);

		var position = createVector(this.starting_point.x, this.starting_point.y);
		this.population[i] = new Vehicle(position, new DNA(), this.population.length, i+1);
	}

	this.live = function (os) {
		for (var i = 0; i < this.population.length; i++) {
			this.population[i].check_target();
			this.population[i].run(os);
		}
	}

	this.target_reached = function () {
		for (var i = 0; i < this.population.length; i++) {
			if (this.population[i].found_path()) {
				return true;
			} else {
				return false;
			}
		}
	}

	this.fitness = function () {
		for (var i = 0; i < this.population.length; i++) {
			this.population[i].fitness();
		}
	}

	//selection function
	//each agent will be pushed at an array called "mating pool" several times depending
	//on it's fitness score. This will create a higher chance for those who are fit
	//and a lower chance of getting selected for those who are unfit
	this.selection = function () {

		this.mating_pool = [];
		var max_fitness = this.get_max_fitness();
		var agent = this.index;
		var Kilometers = this.kilometers;

		for (var i = 0; i < this.population.length; i++) {

			//get the fitness normal of agent[i]
			var fitness_normal = map(this.population[i].get_fitness(), 0, max_fitness, 0, 1);
			this.n.push(fitness_normal);

			var n = int(fitness_normal * 100);

			this.normal_fitness.push(fitness_normal);

			//push agent[i] n times in the mating pool
			for (var j = 0; j < n; j++) {
				this.mating_pool.push(this.population[i]);
			}
			if(this.population[i].get_km() != 0)
			console.log('-- Agent [' + (i+1) + '] fitness:\t' + fitness_normal.toFixed(10) + " \t" + this.population[i].get_km() + "km\t" + j);
			else
			console.log('-- Agent [' + (i+1) + '] fitness:\t' + fitness_normal.toFixed(10) + " \t" + this.population[i].check_distance().toFixed(2) + "km\t" + j);
		}
		if(this.population[agent-1].get_km() != 0)
		console.log('Max Fitness--------------\n-- Agent[' + agent + ']:\t' + max_fitness + '      ' + this.population[agent-1].get_km() + 'km');
		else
		console.log('Max Fitness--------------\n-- Agent[' + agent + ']:\t' + max_fitness + '      ' + this.population[agent-1].check_distance().toFixed(2) + 'km');
		console.log('Mating Pool Count:\t' +this.mating_pool.length);
		// start = !start;
	}

	this.get_normal_fitness = function() {

		for(var i = 0; i < this.population.length; i++) {
			console.log('Agent [' +(i+1)+ ']: \t' + this.normal_fitness[i] + '\t\t\t' +int(this.normal_fitness[i]*100));
		}
	}

	//reproduction function (crossover)
	//select two random genes from the mating pool (actual selection) and set it as the parents
	//once the parents are selected. their genes will crossover to create new children (evolution)
	this.reproduction = function () {

		for (var i = 0; i < this.population.length; i++) {

			var mom = this.mating_pool[int(random(this.mating_pool.length))];
			var dad = this.mating_pool[int(random(this.mating_pool.length))];

			var mom_genes = mom.get_dna();
			var dad_genes = dad.get_dna();

			var child = mom_genes.crossover(dad_genes);

			child.mutate(this.mutation_rate);

			var position = createVector(width - 760, height / 2);

			this.population[i] = new Vehicle(position, child, this.population.length, i);
		}
		this.generations++;
	}

	this.show_mating_pool = function () {

		for(var i = 0; i < this.population.length; i++) {
			console.log('Agent [' +(i+1)+ ']:\t\t' +this.n[i]+ "\t" +this.normal_fitness[i]);
		}
	}

	this.show_genes = function() {
		console.log('--Agent\'s Genes');
		for(var i = 0; i < this.population.length; i++) {
			console.log(this.population[i].show_genes());
		}
	}

	this.show_dna = function() {
		console.log('--Agent\'s DNA');
		for(var i = 0; i < this.population.length; i++) {
			console.log('Agent[' +(i+1)+ ']');
			console.log(this.population[i].get_dna());
		}
	}

	this.get_generations = function () {
		return this.generations + 1;
	}

	this.get_max_fitness = function () {
		var record = 0;
		console.log('-------- Generation: ' + this.get_generations() + ' --------------------------');
		for (var i = 1; i < this.population.length + 1; i++) {
			if (this.population[i - 1].get_fitness() > record) {
				record = this.population[i - 1].get_fitness();
				this.index = i;
			}
		}
		return record;
	}

	//get genes of the fittest chromosome
	this.get_fittest_dna = function() {
		var agent = this.index-1;
		var genes = this.population[agent].show_genes();
		console.log('-- Agent [' +(agent+1)+ ']: DNA');
		for(var i = 0; i < genes.length; i++) {
			console.log(genes[i].x + ', ' + genes[i].y);
		}
		return genes;
	}
}