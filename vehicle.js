
function Vehicle(pos, dna, total_vehicle, num) {

	this.agent_number = num;
	this.fitness = 0;
	this.km;
	this.hit_km = 0;
	this.position = pos.copy();
	this.acceleration = createVector();
	this.velocity = createVector();

	this.r = 3;
	this.dna = dna;
	this.finish_time = 0;
	this.record_dist = 10000;
	this.label = 0;

	this.gene_counter = 0;
	this.hit_obstacle = false;
	this.hit_target = false;
	this.dead = false;
	this.out_of_bounds = false;
	this.path_found = false;

	this.getGenes = dna.getGenes();

	//fitness function
	//calculation of the fitness value for each agent after being evaluated
	this.fitness = function () {
		this.fitness = (1 / (this.finish_time * this.record_dist));
		this.fitness = pow(this.fitness, 4);

		if (this.hit_obstacle || this.borders) this.fitness *= 0.1;  // lose 90% of fitness
		if (this.hit_target) this.fitness *= 2;    // double the fitness
	}

	this.run = function(os) {
		if (!this.hit_obstacle ) {
		  this.apply_force(this.dna.genes[this.gene_counter]);
		  this.gene_counter = (this.gene_counter + 1) % this.dna.genes.length;
		 
		  // If I hit an edge
		  this.borders();
		  
		  // If I hit an obstacle
		  this.obstacles(os);
		  
		  this.update();
		}

		if (!this.hit_target && !this.out_of_bounds) {
			this.display();			
		}

		// Check if vehicle has reached the target
		if (this.hit_target) {
			console.log('Generation ' + (population.get_generations()) + ': possible path found!');
			start = !start;

			if(this.label != 1) {
				createP('Generation ' + (population.get_generations()) + ' Agent['+ (this.agent_number+1) +'] possible path found!');
				var txt = ('Distance of Path by Agent: ' +(this.check_distance().toFixed(2))+'km');
				createP(txt);
				this.hit_km = this.check_distance().toFixed(2);
				this.label++;
			}
		}
	  }

	//edge-case
	this.borders = function () {
		if ((this.position.x < 0) || (this.position.y < 0) || (this.position.x > width) || (this.position.y > height)) {
			this.out_of_bounds = true;
			return true;
		} else {
			return false;
		}
	}

	// check if i hit an obstacle
	this.obstacles = function (os) {
		for (var i = 0; i < os.length; i++) {
			if (os[i].contains(this.position)) {
				console.log('dead');
				this.hit_obstacle = true;
			}
		}
	}

	//checks the distance of the agent from the target's position
	this.check_target = function () {
		var d = dist(this.position.x, this.position.y, target.position.x, target.position.y);
		if (d < this.record_dist) this.record_dist = d;

		if (target.contains(this.position) && !this.hit_target) {
			this.hit_target = true;
		} else if (!this.hit_target) {
			this.finish_time++;
		}
	}


	//checks the distance of the agent from the starting point
	this.check_distance = function() {
		var d = dist(width-760, height/2, this.position.x, this.position.y);
		if(this.position.x < (width-760)) d *= -1;
		d = d*0.01;
		this.km = d;
		return this.km;
	}

	this.apply_force = function (f) {
		this.acceleration.add(f);
	}

	//algorithm or engine for the movement of the agent
	this.update = function () {
		this.velocity.add(this.acceleration);
		this.position.add(this.velocity);
		this.acceleration.mult(0);
	}

	//displays the model for each agent in the simulation
	this.display = function () {
		var theta = this.velocity.heading() + PI / 2;
		fill(200, 100);
		stroke(0);
		strokeWeight(1);
		push();
		translate(this.position.x, this.position.y);
		rotate(theta);
		fill(175);
		beginShape(TRIANGLES);
		vertex(0, -this.r * 2);
		vertex(-this.r, this.r * 2);
		vertex(this.r, this.r * 2);
		endShape();
		pop();
	}

	this.show_genes = function() {
		return this.getGenes;
	}

	this.get_fitness = function () {
		return this.fitness;
	}

	this.get_dna = function () {
		return this.dna;
	}

	this.stopped = function () {
		return this.hit_obstacle;
	}

	this.get_ft = function() {
		return this.finish_time*0.01;
	}

	this.target_status = function() {
		return this.hit_target;
	}

	this.found_path = function() {
		return this.path_found;
	}
	
	this.get_km = function() {
		return this.hit_km;
	}
}